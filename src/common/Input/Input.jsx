import './Imput.css';

export function Input(props) {
	const {
		className,
		inputType,
		placeholderText,
		onChange,
		minLength,
		maxLength,
		value,
		isRequired = false,
	} = props;

	return (
		<input
			className={className}
			type={inputType}
			placeholder={placeholderText}
			minLength={minLength}
			maxLength={maxLength}
			onChange={onChange}
			value={value}
			required={isRequired}
		/>
	);
}
