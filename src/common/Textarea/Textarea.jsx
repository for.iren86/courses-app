export function Textarea(props) {
	const {
		labelText,
		minLength,
		maxLength,
		descriptionText,
		onChange,
		isRequired = false,
	} = props;

	return (
		<div className='description_container'>
			<label>{labelText}</label>
			<textarea
				className='description_field'
				minLength={minLength}
				maxLength={maxLength}
				placeholder={descriptionText}
				onChange={onChange}
				required={isRequired}
			/>
		</div>
	);
}
