import './Button.css';

export function Button(props) {
	const {
		type = 'button',
		size,
		location,
		color,
		text,
		handleClick,
		userID,
		userName,
	} = props;

	return (
		<button
			type={type}
			className={`btn ${size} ${location} ${color}`}
			onClick={handleClick}
			data-id={userID}
			data-name={userName}
		>
			{text}
		</button>
	);
}
