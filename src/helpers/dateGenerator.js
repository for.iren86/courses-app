export const dateGenerator = (date) => {
	let newDate = new Date(date);
	let year = newDate.getFullYear();
	let month = newDate.getMonth() + 1;
	let dt = newDate.getDate();

	if (dt < 10) {
		dt = '0' + dt;
	}
	if (month < 10) {
		month = '0' + month;
	}

	return `${dt}.${month}.${year}`;
};
