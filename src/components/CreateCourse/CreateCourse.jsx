import React, { useState } from 'react';
import { useNavigate } from 'react-router';
import { getId } from '../../helpers/idGenerator';
import { pipeDuration } from '../../helpers/pipeDuration';
import { mockedAuthorsList, mockedCoursesList } from '../../constants';
import { Input } from '../../common/Input/Input';
import { Textarea } from '../../common/Textarea/Textarea';
import { Button } from '../../common/Button/Button';
import './CreateCourse.css';

let authorsList = mockedAuthorsList;
let assignedAuthorsList = [];

function capitalizeFirstLetter(text) {
	return text.charAt(0).toUpperCase() + text.slice(1);
}

function Author(props) {
	return (
		<div className='author'>
			<span className='author_name'>{props.userName}</span>
			<Button
				text={props.buttonText}
				size={props.size}
				userID={props.userID}
				userName={props.userName}
				handleClick={props.handleClick}
				location={props.location}
				color={props.color}
			/>
		</div>
	);
}

function AuthorsList(props) {
	return props.authors.map((el) => {
		return (
			<Author
				buttonText='Add author'
				key={getId()}
				size='btn-small'
				color='orange'
				location='add_author_button'
				userID={el.id}
				userName={el.name}
				handleClick={props.handleClick}
			/>
		);
	});
}

function CourseAuthors(props) {
	return props.authors.map((el) => {
		return (
			<Author
				buttonText='Delete author'
				key={el.id}
				size='btn-small'
				color='grey'
				userID={el.id}
				userName={el.name}
				handleClick={props.handleClick}
			/>
		);
	});
}

function setInitialFormState() {
	assignedAuthorsList = [];
	authorsList = mockedAuthorsList;
}

function TitleComponent(props) {
	return (
		<div className='title_block'>
			<div className='title_input'>
				<label className='label_for_input' htmlFor='create_title'>
					Title
				</label>
				<Input
					id='create_title'
					className='course_title_input'
					placeholderText='Enter title...'
					inputType='text'
					minLength='2'
					maxLength='20'
					isRequired={true}
					onChange={props.handleSetTitle}
				/>
			</div>
			<Button
				type='submit'
				size='btn-small'
				text='Create course'
				location='create_course_button'
				color='berk'
			/>
		</div>
	);
}

function DescriptionComponent(props) {
	return (
		<Textarea
			labelText='Description'
			descriptionText='Enter description'
			minLength='2'
			maxLength='240'
			onChange={props.onChange}
			isRequired={true}
		/>
	);
}

function CreateAuthorComponent(props) {
	return (
		<div className='add_author_block'>
			<h5>Add author</h5>
			<label className='label_for_input' htmlFor='authorName'>
				Author name
			</label>
			<Input
				id='authorName'
				type='text'
				placeholderText='Enter author name...'
				value={props.authorName}
				onChange={props.onChange}
			/>
			<Button
				size='btn-large'
				location='create_author_button'
				text='Create author'
				color='orange'
				handleClick={props.handleClick}
			/>
		</div>
	);
}

function AddAuthorComponent(props) {
	return (
		<div className='authors_block'>
			<h5>Authors</h5>
			<AuthorsList handleClick={props.handleClick} authors={props.authors} />
		</div>
	);
}

function CourseDurationComponent(props) {
	return (
		<div className='course_duration_block'>
			<h5>Duration</h5>
			<Input
				inputType='number'
				location='create_course_duration'
				className='course_duration_input'
				labelText='Duration'
				placeholderText='Enter duration in minutes...'
				minLength='1'
				maxLength='10'
				value={props.durationValue}
				isRequired={true}
				onChange={props.onChange}
			/>
			<span className='duration_time_block'>
				Duration:
				<span className='hours_in_min'>{`${
					pipeDuration(props.durationValue).time
				}`}</span>
				<span className='hours_text'>{`${
					pipeDuration(props.durationValue).text
				}`}</span>
			</span>
		</div>
	);
}

function CourseAuthorsComponent(props) {
	return (
		<div className='course_authors_block'>
			<h5>Course authors</h5>
			{props.authors.length === 0 ? (
				<EmptyAuthorsListComponent defaultValue={props.defaultValue} />
			) : (
				<CourseAuthors
					authors={props.authors}
					handleClick={props.handleClick}
				/>
			)}
		</div>
	);
}

function EmptyAuthorsListComponent() {
	return (
		<span className='empty_authors_list_message'>Author list is empty</span>
	);
}

export function CreateCourse() {
	const DEFAULT_AUTHORS_LIST = 'Author list is empty';
	const MIN_DURATION = 1;

	const [authorsState, setAuthorsState] = useState(authorsList);
	const [appliedAuthorsState, setAppliedAuthorsState] =
		useState(assignedAuthorsList);
	const [title, setTitle] = useState('');
	const [description, setDescription] = useState('');
	const [author, setAuthor] = useState('');
	const [duration, getDuration] = useState(MIN_DURATION);
	let navigate = useNavigate();

	let coursePattern = {
		id: '',
		title: '',
		description: '',
		creationDate: '',
		duration: 0,
		authors: [],
	};

	function applyCourseId() {
		let courseId = getId();
		coursePattern['id'] = `${courseId}`;
	}

	function applyTitle() {
		coursePattern['title'] = capitalizeFirstLetter(title);
	}

	function applyDescription() {
		coursePattern['description'] = capitalizeFirstLetter(description);
	}

	function applyCurrentDate() {
		coursePattern['creationDate'] = new Date().toLocaleDateString();
	}

	function applyDuration(target) {
		if (parseInt(target) > 0) {
			coursePattern['duration'] = parseInt(target);
		}
	}

	function applyCourseAuthors() {
		appliedAuthorsState.forEach((el) => coursePattern['authors'].push(el.id));
	}

	function createCourse(courseItem) {
		mockedCoursesList.push(courseItem);
	}

	function handleSubmit(e) {
		if (isAuthorsSelected(e)) {
			submitForm(e);
		}
	}

	const submitForm = (e) => {
		e.preventDefault();
		applyCourseId();
		applyTitle();
		applyDescription();
		applyCurrentDate();
		applyDuration(duration);
		applyCourseAuthors();
		createCourse(coursePattern);
		navigate('/');
		setInitialFormState();
	};

	function isAuthorsSelected(e) {
		e.preventDefault();
		if (appliedAuthorsState.length === 0) {
			alert('Please select coursePattern author(s)');
			return false;
		} else {
			return true;
		}
	}

	function resetAddAuthorInputText() {
		setAuthor('');
	}

	function createAuthor(userName) {
		let authorId = getId();
		let newAuthor = { id: `${authorId}`, name: userName };
		authorsState.push(newAuthor);
		setAuthorsState(authorsState);
	}

	function addAuthor(userId) {
		let author = authorsState.find((el) => el.id === userId);
		appliedAuthorsState.push(author);
		setAuthorsState(authorsState.filter((el, index) => el['id'] !== userId));
	}

	function removeAuthor(userId) {
		let author = appliedAuthorsState.find((el) => el.id === userId);
		authorsState.push(author);
		setAppliedAuthorsState(
			appliedAuthorsState.filter((el, index) => el['id'] !== author['id'])
		);
	}

	function handleCreateAuthor(e) {
		e.preventDefault();
		if (author.length > 2) {
			createAuthor(author);
			resetAddAuthorInputText();
		}
	}

	function handleAddAuthor(e) {
		e.preventDefault();
		const userId = e.target.getAttribute('data-id');
		addAuthor(userId);
	}

	function handleSetDuration(e) {
		if (e.target.value.length > 10) {
			e.target.value = e.target.value.slice(0, 10);
		}
		if (parseInt(e.target.value) > 0) {
			getDuration(e.target.value);
		}
	}

	function handleApplyAuthor(e) {
		e.preventDefault();
		const userId = e.target.getAttribute('data-id');
		removeAuthor(userId);
	}

	return (
		<div className='create_course_container_flex'>
			<form
				className='form'
				onSubmit={(e) => {
					return handleSubmit(e);
				}}
			>
				<TitleComponent handleSetTitle={(e) => setTitle(e.target.value)} />
				<DescriptionComponent
					onChange={(e) => {
						setDescription(e.target.value);
					}}
				/>
				<div className='create_course_container'>
					<CreateAuthorComponent
						authorName={author}
						onChange={(e) => setAuthor(e.target.value)}
						handleClick={(e) => handleCreateAuthor(e)}
					/>
					<AddAuthorComponent
						authors={authorsState}
						handleClick={(e) => handleAddAuthor(e)}
					/>
					<CourseDurationComponent
						durationValue={duration}
						onChange={(e) => handleSetDuration(e)}
					/>
					<CourseAuthorsComponent
						authors={appliedAuthorsState}
						defaultValue={DEFAULT_AUTHORS_LIST}
						handleClick={(e) => handleApplyAuthor(e)}
					/>
				</div>
			</form>
		</div>
	);
}
