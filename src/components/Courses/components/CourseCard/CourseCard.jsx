import { Button } from '../../../../common/Button/Button';
import './CourseCard.css';

export function CourseCard(props) {
	const { title, description, authors, duration, created } = props;

	return (
		<div className='course_card_container_flex'>
			<div className='course_card_content'>
				<h3 className='course_card_title'>{title}</h3>
				<p>{description}</p>
			</div>
			<div className='course_card_details'>
				<ul>
					<li>
						<b>Authors:</b> {authors}
					</li>
					<li>
						<b>Duration:</b> {duration}
					</li>
					<li>
						<b>Created:</b> {created}
					</li>
				</ul>
				<Button
					size='btn-small'
					text='Show course'
					location='course_card_button'
					color='grey'
				/>
			</div>
		</div>
	);
}
