import { Input } from '../../../../common/Input/Input';
import { Button } from '../../../../common/Button/Button';
import './SearchBar.css';

export function SearchBar(props) {
	return (
		<div className='searchbar_container_flex'>
			<Input
				className='searchbar_input'
				placeholderText='Enter course name...'
				onChange={props.onChange}
			/>
			<Button
				size='btn-large'
				text='Search'
				location='searchbar_button'
				color='berk'
			/>
		</div>
	);
}
