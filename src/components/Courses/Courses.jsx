import React, { useState } from 'react';
import { pipeDuration } from '../../helpers/pipeDuration';
import { dateGenerator } from '../../helpers/dateGenerator';
import { mockedCoursesList, mockedAuthorsList } from '../../constants';
import { SearchBar } from './components/SearchBar/SearchBar';
import { CourseCard } from './components/CourseCard/CourseCard';
import { Link } from 'react-router-dom';
import './Courses.css';

function generateCard(card) {
	return (
		<CourseCard
			key={card.id}
			title={card.title}
			description={card.description}
			authors={getAuthors(card.authors, mockedAuthorsList)}
			duration={`${pipeDuration(card.duration).time} ${
				pipeDuration(card.duration).text
			}`}
			created={dateGenerator(card.creationDate)}
		/>
	);
}

function findCourse(text, card) {
	if (text === '') {
		return card;
	} else if (card.title.includes(text)) {
		return card;
	} else if (card.id.includes(text)) {
		return card;
	}
	return false;
}

export function Courses() {
	const [query, setQuery] = useState('');

	return (
		<div className='courses_container_flex'>
			<SearchBar onChange={(event) => setQuery(event.target.value)} />
			<Link to='/create-course' className='btn btn-large orange' type='button'>
				Add new course
			</Link>

			{mockedCoursesList
				.filter((card) => findCourse(query, card))
				.map((el) => generateCard(el))}
		</div>
	);
}

function getAuthors(authorsList, allAuthors) {
	let authorsNamesList = '';
	for (let i = 0; i < authorsList.length; i++) {
		let author = allAuthors.find((el) => authorsList[i] === el.id);
		if (authorsNamesList.length === 0) {
			authorsNamesList += author.name;
		} else {
			authorsNamesList += ', ' + author.name;
		}
	}
	return authorsNamesList;
}
