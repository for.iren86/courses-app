import { Logo } from './components/Logo/Logo.jsx';
import { Button } from '../../common/Button/Button';
import './Header.css';

export function Header() {
	return (
		<div className='header_flex_container'>
			<Logo className='header_logo' />
			<div className='header_user_block'>
				<span className='header_user_name'>Dave</span>
				<Button
					size='btn-small'
					text='Log out'
					location='header_button'
					color='orange'
				/>
			</div>
		</div>
	);
}
