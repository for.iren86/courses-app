import { Header } from './components/Header/Header';
import { Courses } from './components/Courses/Courses';
import { CreateCourse } from './components/CreateCourse/CreateCourse';
import { BrowserRouter, Routes, Route } from 'react-router-dom';

function App() {
	return (
		<BrowserRouter>
			<Header />
			<Routes>
				<Route exact path='/' element={<Courses />} />
				<Route exact path='/create-course' element={<CreateCourse />} />
			</Routes>
		</BrowserRouter>
	);
}
export default App;
